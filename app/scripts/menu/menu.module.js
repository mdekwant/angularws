(function () {
  'use strict';

  angular.module('angularjsles.menu.module', [
    'angularjsles.shared.module'
  ]);

})();
