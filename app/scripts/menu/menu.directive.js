(function () {
  'use strict';

  angular.module('angularjsles.menu.module').directive('topMenu', topMenu);

  topMenu.$inject = [];

  function topMenu() {

    return {
      scope: {},
      restrict: 'E',
      templateUrl: 'scripts/menu/menu.html',
      replace: true
    };
  }
})();
