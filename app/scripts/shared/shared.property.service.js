(function () {
  'use strict';

  angular.module('angularjsles.shared.module')
    .config(configure);

  configure.$inject = ['propertyServiceProvider'];

  function configure(propertyServiceProvider) {
    propertyServiceProvider.add('angularjsles.shared.module', {
      labels: {
        appName: 'angularJsLes'
      }
    });
  }

})();
