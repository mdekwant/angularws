(function () {
  'use strict';

  angular.module('angularjsles.shared.module')
    .provider('propertyService', propertyService);

    function propertyService() {
      this.properties = {};

      function replaceVariables(message, variables) {
        angular.forEach(variables, function (variable, index) {
          message = message.split('{' + index + '}').join(variables[index]);
        });

        return message;
      }

      this.$get = function () {
        var properties = this.properties;
        return {
          property: function (module, key) {
            return properties[module][key];
          },
          properties: function (module) {
            return properties[module];
          },
          replaceVariables: replaceVariables
        };
      };

      this.add = function (module, objects) {
        var properties = this.properties;
        angular.forEach(objects, function (value, key) {
          if (angular.isUndefined(properties[module])) {
            properties[module] = {};
          }
          properties[module][key] = value;
        });
      };
    }

})();
