(function () {
  'use strict';

  angular.module('angularjsles.login.module', [
    'angularjsles.shared.module'
  ]);

})();
