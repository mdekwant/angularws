(function () {
  'use strict';

  angular

    // Register all used modules by this application
    .module('angularjsles', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'ngTouch',
      'angularjsles.about.module',
      'angularjsles.home.module',
      'angularjsles.shared.module',
      'angularjsles.menu.module',
      'angularjsles.portfolio.module',
      'angularjsles.about.testimonials.module'
    ])
    .config(function ($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'scripts/home/home.html',
          controller: 'HomeController',
          controllerAs: 'vm'
        })
        .when('/about', {
          templateUrl: 'scripts/about/about.html',
          controller: 'AboutController',
          controllerAs: 'vm'
        })
        .when('/portfolio', {
          templateUrl: 'scripts/portfolio/portfolio.html',
          controller: 'PortfolioController',
          controllerAs: 'vm'
        })
        .when('/error', {
          templateUrl: 'error.html'
        })
        .otherwise({
          redirectTo: '/'
        });
    });

})();
