(function () {
  'use strict';

  angular.module('angularjsles.about.module')
    .config(configure);

  configure.$inject = ['propertyServiceProvider'];

  function configure(propertyServiceProvider) {
    propertyServiceProvider.add('angularjsles.about.module', {
      labels: {
        title: 'About'
      },
      content: {
        mainText: 'term gras of grasachtige plant kan voor andere groene planten met lijnvormige bladeren gebruikt ' +
        'worden, buiten de leden van de grassenfamilie, terwijl sommige grassoorten een voor de familie minder ' +
        'karakteristiek vorm hebben. [Wikipedia]',
        mainTextFirst: 'De'
      }
    });
  }

})();
