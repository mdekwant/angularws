(function () {
  'use strict';

  angular.module('angularjsles.about.module', [
    'angularjsles.shared.module'
  ]);

})();
