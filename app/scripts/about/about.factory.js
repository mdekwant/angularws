(function () {
  'use strict';

  angular.module('angularjsles.about.module')
    .factory('AboutFactory', AboutFactory);

  AboutFactory.$inject = ['$resource'];

  function AboutFactory($resource) {
    return $resource('https://putsreq.com/0aIdHFQJYQipveUbnrEW', {}, {
      getValidUserIds: {method:'GET', params: {name: 'validUserIds'}},
      getUserComments: {method:'POST', params: {name: 'users', ids: '@ids'}}
    });
  }
})();
