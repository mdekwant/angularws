(function () {
  'use strict';

  angular.module('angularjsles.about.testimonials.module').directive('aboutTestimonials', aboutTestimonials);

  aboutTestimonials.$inject = [];

  function aboutTestimonials() {
    return {
      scope: {},
      restrict: 'E',
      bindToController: {
        filter: '=validUserIds'
      },
      templateUrl: 'scripts/about/testimonials/about.testimonials.html',
      controller: 'AboutTestimonialsController',
      controllerAs: 'vm',
      replace: true
    };
  }
})();
