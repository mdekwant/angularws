(function () {
  'use strict';

  angular.module('angularjsles.about.testimonials.module')
    .controller('AboutTestimonialsController', AboutTestimonialsController);

  AboutTestimonialsController.$inject = ['propertyService', 'AboutFactory', '$log', '$scope'];

  function AboutTestimonialsController(propertyService, aboutFactory, $log, $scope) {
    var vm = this;

    vm.testimonialItems = [];
    vm.properties = $.extend(true,
      {},
      propertyService.properties('angularjsles.shared.module'),
      propertyService.properties('angularjsles.about.module'),
      propertyService.properties('angularjsles.about.testimonials.module')
    );

    // Watches komem in meerdere smaken, deze is speciaal voor een collection,
    //  deze houdt ook mutaties qua inhoud van de collecties in de gaten.
    // Als je alleen een object in de gaten wil houden kun je $scope.$watch() gebruiken.
    //  hou wel rekening dat het object niet undefined mag zijn, anders heb je initeel niets om te volgen.
    $scope.$watchCollection('vm.filter', update);

    function update(newValue) {
      if(angular.isDefined(newValue) && newValue.length > 0) {
        aboutFactory.getUserComments({ids: (vm.filter.join(','))}, onUserCommentsSuccess, onUserCommentsFailure);
      }
    }

    function onUserCommentsSuccess(result) {
      vm.testimonialItems = result.data;
    }

    function onUserCommentsFailure(result) {
      vm.error = result.data;
      $log.error(result);
      $scope.$broadcast('errorRegistered');
    }
  }

})();
