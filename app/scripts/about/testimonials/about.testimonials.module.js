(function () {
  'use strict';

  angular.module('angularjsles.about.testimonials.module', [
    'angularjsles.shared.module',
    'angularjsles.about.module'
  ]);

})();
