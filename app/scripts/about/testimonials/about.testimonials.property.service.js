(function () {
  'use strict';

  angular.module('angularjsles.about.testimonials.module')
    .config(configure);

  configure.$inject = ['propertyServiceProvider'];

  function configure(propertyServiceProvider) {
    propertyServiceProvider.add('angularjsles.about.testimonials.module', {
      labels: {
        subTitle: 'User testimonials'
      }
    });
  }

})();
