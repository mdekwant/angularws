(function () {
  'use strict';

  angular.module('angularjsles.about.module')
    .controller('AboutController', AboutController);

  AboutController.$inject = ['propertyService', 'AboutFactory', '$log', '$scope'];

  function AboutController(propertyService, aboutFactory, $log, $scope) {
    var vm = this;

    vm.validUserIds = [];
    vm.properties = $.extend(true,
      propertyService.properties('angularjsles.shared.module'),
      propertyService.properties('angularjsles.about.module')
    );

    (function init(){
      aboutFactory.getValidUserIds({}, onValidUserIdsSuccess, onValidUserIdsFailure);
    })();

    function onValidUserIdsSuccess(result) {
      vm.validUserIds = result.data;
    }

    function onValidUserIdsFailure(result) {
      vm.error = result.data;
      $log.error(result);
      $scope.$broadcast('errorRegistered');
    }
  }

})();
