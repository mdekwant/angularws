(function () {
  'use strict';

  angular.module('angularjsles.error.module', [
    'angularjsles.shared.module'
  ]);

})();
