(function () {
  'use strict';

  angular.module('angularjsles.error.module')
    .controller('ErrorController', ErrorController);

  ErrorController.$inject = ['propertyService', '$scope', '$location'];

  function ErrorController(propertyService, $scope, $location) {
    var vm = this;

    vm.properties = $.extend(true,
      {},
      propertyService.properties('angularjsles.shared.module'),
      propertyService.properties('angularjsles.error.module')
    );

    vm.closeError = closeError;

    // Listen on scope
    $scope.$on('errorRegistered', errorFound);

    function closeError() {
      $scope.$emit('errorClosed');
    }

    function errorFound() {
      $location.path('error');
    }
  }

})();
