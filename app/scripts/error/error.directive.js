(function () {
  'use strict';

  angular.module('angularjsles.error.module').directive('errorHandler', errorHandler);

  errorHandler.$inject = [];

  function errorHandler() {
    return {
      scope: {},
      restrict: 'E',
      bindToController: {},
      templateUrl: 'error.html',
      controller: 'ErrorController',
      controllerAs: 'vm',
      replace: true
    };
  }
})();
