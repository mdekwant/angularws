(function () {
  'use strict';

  angular.module('angularjsles.portfolio.module')
    .controller('PortfolioController', PortfolioController);

  PortfolioController.$inject = ['propertyService', 'PortfolioFactory', '$log', '$scope'];

  function PortfolioController(propertyService, portfolioFactory, $log, $scope) {
    var vm = this;

    vm.portfolioItems = [];
    vm.properties = $.extend(true,
      propertyService.properties('angularjsles.shared.module'),
      propertyService.properties('angularjsles.portfolio.module')
    );

    vm.errorOne = errorOne;
    vm.errorTwo = errorTwo;

    (function init(){
      portfolioFactory.getAll({}, onPortfolioSuccess, onPortfolioFailure);
    })();

    function onPortfolioSuccess(result) {
      vm.portfolioItems = result.data;
    }

    function onPortfolioFailure(result) {
      genericFailure(result);
    }

    function errorOne() {
      portfolioFactory.errorOne({}, genericFailure, genericFailure);
    }

    function errorTwo() {
      portfolioFactory.errorTwo({}, genericFailure, genericFailure);
    }

    function genericFailure(result) {
      vm.error = result.data;
      $scope.$broadcast('errorRegistered');
    }
  }

})();
