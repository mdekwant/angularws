(function () {
  'use strict';

  angular.module('angularjsles.portfolio.module', [
    'angularjsles.shared.module'
  ]);

})();
