(function () {
  'use strict';

  angular.module('angularjsles.portfolio.module').directive('portfolioItem', portfolioItem);

  portfolioItem.$inject = [];

  function portfolioItem() {

    return {
      scope: {item: '=' },
      restrict: 'E',
      templateUrl: 'scripts/portfolio/item/portfolio.item.html',
      replace: true
    };
  }
})();
