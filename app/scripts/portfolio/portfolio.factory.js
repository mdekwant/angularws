(function () {

  'use strict';

  angular.module('angularjsles.portfolio.module')
    .factory('PortfolioFactory', PortfolioFactory);

  PortfolioFactory.$inject = ['$resource'];

  function PortfolioFactory($resource) {
    var baseUrl = 'https://putsreq.com/0aIdHFQJYQipveUbnrEW';
    return $resource('', {}, {
      getAll: {url: baseUrl, method:'POST', params: {name: 'items'}},
      errorOne: {url: baseUrl, method:'POST', params: {name: 'errorPage'}},
      errorTwo: {url: baseUrl, method:'POST', params: {name: 'errorServer'}}
    });
  }
})();
