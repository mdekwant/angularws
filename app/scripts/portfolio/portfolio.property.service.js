(function () {
  'use strict';

  angular.module('angularjsles.portfolio.module')
    .config(configure);

  configure.$inject = ['propertyServiceProvider'];

  function configure(propertyServiceProvider) {
    propertyServiceProvider.add('angularjsles.portfolio.module', {
      labels: {
        title: 'Portfolio',
        cam_title: 'Live-cam winning'
      }
    });
  }

})();
