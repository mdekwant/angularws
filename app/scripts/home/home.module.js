(function () {
  'use strict';

  angular.module('angularjsles.home.module', [
    'angularjsles.shared.module'
  ]);

})();
