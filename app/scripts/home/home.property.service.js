(function () {
  'use strict';

  angular.module('angularjsles.home.module')
    .config(configure);

  configure.$inject = ['propertyServiceProvider'];

  function configure(propertyServiceProvider) {
    propertyServiceProvider.add('angularjsles.home.module', {
      labels: {
        title: 'GroenGras'
      },
      content: {
        quote: '"Beschikbaar in elke kleur zolang het maar groen is." - Harrie Fjord',
        mainText: 'hernieuwbaar gras dat op duurzame wijze is opgewekt en dat is opgewerkt zodat het dezelfde ' +
        'kwaliteit heeft als aardgras. Vanaf 1 oktober 2014 moet het in Nederland voldoen aan de Ministriële ' +
        'regeling gras  kwaliteit. Groen gras   is methaanhoudend gras   dat duurzaam is geproduceerd, bijvoorbeeld ' +
        'uit biomassa en kan dienen als alternatief voor aardgras. [Wikipedia]',
        mainTextFirst: 'Is'
      }
    });
  }

})();
