(function () {
  'use strict';

  angular.module('angularjsles.home.module')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['propertyService'];

  function HomeController(propertyService) {
    var vm = this;

    vm.properties = $.extend(true,
      propertyService.properties('angularjsles.shared.module'),
      propertyService.properties('angularjsles.home.module')
    );

  }

})();
