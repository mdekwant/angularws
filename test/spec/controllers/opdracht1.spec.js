/**
 * Opdracht 1, Het rechtrekken van een ongelukkige constructie (refactoring).
 *
 * Deze opdracht bestaat uit het lostrekken van 'User testimonials' uit de about page
 * Het doel is een nieuwe pagina aan te maken waar deze berijkbaar zijn.
 *
 * Stappen:
 *  1.1. Aanmaken nieuwe map in app/scripts/.. genaamt testimonials
 *  1.2. Aanmaken nieuwe module ('angularjsles.about.testimonials.module') en deze registeren in app.js.
 *  1.3. Verhuizen en aanpassen bestaande 'angularjsles.about.testimonials.module'
 *    1.3.1 Let op bij het aanmaken van een nieuwe pagina dient er ook te worden gekeken naar routing (app.js).
 *  1.4. Verhuizen van about.factory.js en filtering in AboutController
 *    1.4.1 Let op er staat code voor filtering in de About controller, wat moet je hier voor aanpassen?
 */
describe('Opdracht 1. Controller: AboutTestimonialsController', function () {
  'use strict';

  // Create a new module combining http management and angularjsles modules
  angular.module('angularjslesMock', ['backMock', 'angularjsles']);

  // load the controller's module
  beforeEach(module('angularjslesMock'));

  var controller, oldController, $scope, serviceMock, oldServiceMock;

  function setupAngularDependencies() {
    // Maak een dummy $scope om later te injecten
    inject(function ($injector) {
      $scope = $injector.get('$rootScope').$new();
    });
  }

  beforeEach(setupAngularDependencies);

  describe('------', function(){
    beforeEach(function () {
      setupDefaultMocks();
      setupController();
      $scope.$digest();
    });

    // Test opdracht 1.2.
    // Hier wordt simpel gekeken of een specifieke module bestaat.
    it('1.2 - Thew new module should be defined', function() {
      var result;
      try { angular.module('angularjsles.testimonials.module'); } catch(error) { result = error; }
      expect(result).toBeUndefined();
    });

    // Test opdracht 1.3.
    it('1.3 - The (new) controller should be defined and/or the old be renamed and removed', function () {
      expect(oldController).toBeUndefined();
      expect(controller).toBeDefined();
    });

    // Test opdracht 1.4.
    it('1.4 - The Service and filtering should be moved and handled', function () {
      expect(controller).toBeDefined();

      // Test the running of the right service
      expect(serviceMock.getValidUserIds).toHaveBeenCalled();
      expect(serviceMock.getUserComments).toHaveBeenCalled();
      expect(oldServiceMock.getValidUserIds).not.toHaveBeenCalled();
      expect(oldServiceMock.getUserComments).not.toHaveBeenCalled();
    });

  });

  // Voor mocken wordt er gebruik gemaakt van jasmine, kijk daar voor meer informatie.
  function setupDefaultMocks() {
    createAboutFactoryMock();
    createTestimonialFactoryMock();
  }

  // Initialize the controller
  function setupController() {
    // Hier kan je de gemockte onderdelen in de controller injecteren.
    // Als je een specifiek onderdeel niet injecteerd, wordt de aanwezige echte gebruikt, indien aanwezig
    inject(function ($controller) {
      // Maak de controllers aan om te testen of de migratie gelukt is
      try {
        controller = $controller('TestimonialsController', {
          $scope: $scope,
          'AboutFactory': oldServiceMock,
          'TestimonialsFactory': serviceMock
        });
      } catch (error) {}

      try {
        oldController = $controller('AboutTestimonialsController', {
          $scope: $scope,
          'AboutFactory': oldServiceMock,
          'TestimonialsFactory': serviceMock
        });
      } catch (error) {}
    });
  }

  // Initialize the old mocks
  function createAboutFactoryMock() {
    oldServiceMock = jasmine.createSpyObj('AboutFactory', ['getValidUserIds', 'getUserComments']);

    // Er zijn hier meerdere opties mogelijk achter .and. zie
    oldServiceMock.getValidUserIds.and.callFake(function () {
      // Haal alle argumenten op gebruikt bij deze functie in array vorm.
      var args = oldServiceMock.getValidUserIds.calls.allArgs();
      // Door de args.length - 1 ste index te pakken weet je altijd dat je de laatste aanroep te pakken hebt.
      // Van de laatste aanroep, pak het 2e element en voer dit uit (De success functie meegegeven)
      args[args.length - 1][1](angular.copy(validUserIds));

      // Scope $digest geeft aan dat de opgeboude queue van scope events weg werkt en de juiste triggers uitvoerd.
      // Dit is normaal niet nodig, alleen met testen is het nodig deze met de hand uit te voeren.
      // Achter de schermen gaat het om het triggeren van de $scope.$watchCollection('vm.filter'); in de nieuwe -
      //    TestimonialsController.
      $scope.$digest();
    });
    oldServiceMock.getUserComments.and.callFake(function () {
      var args = oldServiceMock.getUserComments.calls.allArgs();
      args[args.length - 1][1](angular.copy(userComments));
    });
  }

  // Initialize the new mocks, voor info zie boven.
  function createTestimonialFactoryMock() {
    serviceMock = jasmine.createSpyObj('TestimonialFactory', ['getValidUserIds', 'getUserComments']);
    serviceMock.getValidUserIds.and.callFake(function () {
      var args = serviceMock.getValidUserIds.calls.allArgs();
      args[args.length - 1][1](angular.copy(validUserIds));
      $scope.$digest();
    });
    serviceMock.getUserComments.and.callFake(function () {
      var args = serviceMock.getUserComments.calls.allArgs();
      args[args.length - 1][1](angular.copy(userComments));
    });
  }

  var validUserIds = {
    data: [2, 3, 5]
  };

  var userComments = {
    data: [
      {
        name: 'Duin. Gras',
        quote: 'Gestopt met het hele "te hooi en te gras"'
      }, {
        name: 'Emmer Tarwe',
        quote: 'Ze nemen nooit te veel hooi op hun vork."'
      }
    ]
  };

});


