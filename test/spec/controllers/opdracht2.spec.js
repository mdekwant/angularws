/**
 * Opdracht 2. Het probleem met problemen.
 *
 * 2.1 Op dit moment worden er geen errors/failures meer weergegeven. Er is wel een error module maar deze doet niets.
 * Hoe kunnen we dit repareren en hoe kunnen we dit aanpakken. Testen kan door de twee error knoppen op de Portfolio pagina.
 *
 *  - Denk aan hoe scopes werken. hoe zit dit elkaar dwars?
 *  - Bouw een eigen oplossing. Pas je het broadcast / emit systeem aan of de Error Directive zelf?
 *  - Hoe zou je error waardes en beschrijvingen doorgeven?
 *
 * 2.2 Nu je een eigen oplossing hebt gemaakt dien je deze ook te testen. Kijk bijvoorbeeld in de testen van opdracht 1,
 * of in de andere testen aanwezig in Feniks. Maar er één naar eigen inzicht.
 */

// Het uit en aanzetten van testen kan gedaan worden om de testen wat meer de focussen
//  - Door een x voor een 'describe' of 'it' functie worden deze test en onderliggende testen uitgesloten
//  - Door een f voor een 'describe' of 'it' functie worden deze testen geforceerd uitgevoerd en de andere genegeerd.
xdescribe('Opdracht 2. Controller: ErrorController', function () {
  'use strict';

  angular.module('angularjslesMock', ['backMock']);

  // load the controller's module
  beforeEach(module('angularjslesMock'));

  var errorController, $scope;

  function setupAngularDependencies() {
    // Maak een dummy $scope om te injecten
    inject(function ($injector) {
      $scope = $injector.get('$rootScope').$new();
    });
  }

  // Initialize the controller and a mock scope
  beforeEach(setupAngularDependencies);

  describe('------', function() {
    beforeEach(function () {
      setupDefaultMocks();
      setupController();
      $scope.$digest();
    });

    it('2.2 Je eigen test', function () {
      expect(errorController).toBeDefined();
    });
  });

  function setupController() {
    // Hier kan je de gemockte onderdelen in de controller injecteren.
    // Als je een specifiek onderdeel niet injecteerd, wordt de aanwezige echte gebruikt, indien aanwezig
    inject(function ($controller) {
      errorController = $controller('ErrorController', {
        $scope: $scope
      });
    });
  }

  function setupDefaultMocks() {
    // Mocks here
  }

});
