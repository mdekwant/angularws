(function () {
  'use strict';

  var app = angular.module('httpbackend.mock', ['ngMockE2E']);

  app.run(runFuction);

  runFuction.$inject = ['$rootScope', '$window', '$location', 'scenarioManager'];

  function runFuction($rootScope, $window, $location, scenarioManager) {

    var count = 0, oldSearch;

    function hasDefinedScenario(key, value) {
      var scenarios = scenarioManager.getDefinedScenarios(),
        scenarioName = {}, hasScenario = false;
      scenarioName[key] = value;
      angular.forEach(scenarios, function (scenario) {
        hasScenario = hasScenario || scenario.matchName(scenarioName);
      });
      return hasScenario;
    }

    function isDelaySearchKey(key) {
      return key.indexOf('delay') > -1;
    }

    function hasChangedScenario() {
      var search = $location.search(),
        returnValue = false;

      for (var searchKey in search) {
        returnValue = returnValue ||
          (oldSearch[searchKey] !== search[searchKey] && (
          hasDefinedScenario(searchKey, oldSearch[searchKey]) ||
          hasDefinedScenario(searchKey, search[searchKey]) ||
          isDelaySearchKey(searchKey)));
      }
      return returnValue;
    }

    $rootScope.$on('$locationChangeSuccess', function () {
      if (count > 0) {
        if (hasChangedScenario()) {
          $window.location.reload();
        }
      }
      oldSearch = $location.search();
      count++;
    });
  }
})();

/**
 * @ngdoc object
 * @name httpbackend.mock.$httpBackend
 * @description The httpbackend.mock module redefines the $httpBackend provided by angular-mock.
 * All methods are republished, so no functionality is lost.
 * The scenario method is added which can be used to define different scenario's for values on the search url (the part after #?)
 *
 * NOTE: This module should only be used when mocking e2e tests!
 */

/**
 * @ngdoc function
 * @name when
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#when when}
 */

/**
 * @ngdoc function
 * @name whenGET
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#whenGET whenGET}
 */

/**
 * @ngdoc function
 * @name whenPOST
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#whenPOST whenPOST}
 */

/**
 * @ngdoc function
 * @name whenHEAD
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#whenHEAD whenHEAD}
 */


/**
 * @ngdoc function
 * @name whenDELETE
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#whenDELETE whenDELETE}
 */


/**
 * @ngdoc function
 * @name whenPUT
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#whenPUT whenPUT}
 */


/**
 * @ngdoc function
 * @name whenJSONP
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#whenJSONP whenJSONP}
 */

/**
 * @ngdoc function
 * @name expect
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#expect expect}
 */

/**
 * @ngdoc function
 * @name expectGET
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#expectGET expectGET}
 */

/**
 * @ngdoc function
 * @name expectPOST
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#expectPOST expectPOST}
 */

/**
 * @ngdoc function
 * @name expectHEAD
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#expectHEAD expectHEAD}
 */


/**
 * @ngdoc function
 * @name expectDELETE
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#expectDELETE expectDELETE}
 */


/**
 * @ngdoc function
 * @name expectPUT
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#expectPUT expectPUT}
 */


/**
 * @ngdoc function
 * @name expectJSONP
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#expectJSONP expectJSONP}
 */

/**
 * @ngdoc function
 * @name flush
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#flush flush}
 */

/**
 * @ngdoc function
 * @name verifyNoOutstandingExpectation
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#verifyNoOutstandingExpectation verifyNoOutstandingExpectation}
 */

/**
 * @ngdoc function
 * @name verifyNoOutstandingRequest
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#verifyNoOutstandingRequest verifyNoOutstandingRequest}
 */

/**
 * @ngdoc function
 * @name resetExpectations
 * @methodOf httpbackend.mock.$httpBackend
 * @description
 * See {@link https://docs.angularjs.org/api/ngMock/service/$httpBackend#resetExpectations resetExpectations}
 */

(function () {
  'use strict';

  angular.module('httpbackend.mock').config(config);

  config.$inject = ['$provide'];

  function config($provide) {

    var $realHttpBackend, $location, scenarioManager, timeout;

    $provide.decorator('$httpBackend', ['$delegate', '$location', 'scenarioManager', 'timeout',
      function ($delegate, _$location_, _scenarioManager_, _timeout_) {
        $realHttpBackend = $delegate;
        $location = _$location_;
        scenarioManager = _scenarioManager_;
        timeout = _timeout_;

        var proxy = createHttpBackend();
        publishHttpBackendMethods(proxy);
        publishScenarioMethods(proxy);
        return proxy;
      }]);

    /**
     * @description
     * Retrieves the delay for given matched scenario name usign the query string variables
     * */
    function getDelay(matchedScenarioName) {
      /*jshint camelcase:false*/
      var search = $location.search(),
        DEFAULT_DELAY = 0;

      function chooseDelayCandidate() {
        for (var i = 0; i < arguments.length; i++) {
          if (angular.isDefined(arguments[i])) {
            return arguments[i];
          }
        }
      }

      return chooseDelayCandidate(
        search['_ib_delay_' + matchedScenarioName],
        search['delay_' + matchedScenarioName],
        search._ib_delay,
        search.delay,
        DEFAULT_DELAY);
    }

    /**
     * Creates the new httpBackend which will function as a proxy for the old one.
     * */
    function createHttpBackend() {

      return function (method, url, data, callback, headers) {
        var _this = this,
          scenarioResponse = scenarioManager.getResponse($location.search(), method, url, data, headers),
          scenarioInterception;

        scenarioInterception = function () {
          if (scenarioResponse) {
            if (scenarioResponse.response) {
              var responseHeaderLines = [];
              angular.forEach(scenarioResponse.response[2], function (value, key) {
                responseHeaderLines.push(key + ': ' + value);
              });
              callback(scenarioResponse.response[0], scenarioResponse.response[1], responseHeaderLines.join('\n'));
            } else {
              // response is a url. Add a when statement for the url to be passed through to te backend
              $realHttpBackend.when(scenarioResponse.method || method, scenarioResponse.forward, data, headers).passThrough();
              // Now let the default implementation of $httpBackend handle the request
              $realHttpBackend.call(_this, scenarioResponse.method || method, scenarioResponse.forward, data, callback, headers);
            }
          } else {
            // Let the default implementation if $httpBackend handle this request
            $realHttpBackend.call(_this, method, url, data, callback, headers);
          }
        };

        // Only delay the response if it is in fact a request matching a predefined scenario
        if (scenarioResponse) {
          timeout(scenarioInterception, getDelay(scenarioResponse.name), false);
        } else {
          scenarioInterception();
        }
      };
    }

    /**
     * Publishes all 'old' http backend methods on the proxy (when, expect, whenGET, expectGET, etc)
     * */
    function publishHttpBackendMethods(httpBackendProxy) {
      for (var i in $realHttpBackend) {
        httpBackendProxy[i] = $realHttpBackend[i];
      }
    }


    /**
     * @ngdoc function
     * @name scenarioGET
     * @methodOf httpbackend.mock.$httpBackend
     * @description
     * Creates a new backend definition for GET used in a specific scenario. For more info see scenario.
     *
     * @param {string|Object} name the name of the scenario. Either a string representing matching the search url key,
     * or an object with one (or more) properties representing the search url key and value pair matching to the search url key and value.
     * @param {string|Object} url HTTP url.
     * @param {(Object|function(Object))=} headers HTTP headers.
     * @returns {requestHandler} Returns an object with `respond`, `passThrough` and `forward` methods that controls how a matched
     * request is handled.
     */

    /**
     * @ngdoc function
     * @name scenario
     * @methodOf httpbackend.mock.$httpBackend
     * @description Like $httpBackend.when, creates a new backend definition, but to be used in a specific case.
     * @param {string|Object} name The name specifying this scenario. Can be a string or an object (for key/value matching).
     * @param {string} method HTTP method
     * @param {string|RegExp} url HTTP url.
     * @param {(string|RegExp|function(string))=} data HTTP request body or function that receives
     *   data string and returns true if the data is as expected.
     * @param {(Object|function(Object))=} headers HTTP headers or function that receives http header
     *   object and returns true if the headers match the current definition.
     * @returns {requestHandler} Returns an object with 'respond', 'passThrough' and 'forward' methods that controls how a matched
     *   request is handled.
     *
     *  - respond –
     *      `{function([status,] data[, headers])`|
   *      `function(method, url, data, headers)}`
     *    – The respond method takes a set of static data to be returned or a function that can return
     *    an array containing response status (number), response data (string) and response headers
     *    (Object).
     *  - passThrough –
     *      `{function()}`
     *    – Specifies that the request is to be passed through for real.
     *  - forward –
     *      `{function(string mockDirectoryPath[, string])}`
     *    – Specifies that the request is to be rewritten and then passed through. Provide the path to the
     *      folder containing the json files which represent different scenario's given the scenario value (on the search url)
     *      The second parameter is optional and is used to define the http method which should be used when forwarding this scenario (default 'GET').
     *      Example: <br />
     *      <pre>
     *        $httpBackend.scenarioGET('accounts', '/app/p-manage-creditcards/authorizedaccounts')
     *          .forward('test/mocks/accounts');
     *      </pre>
     *  - rewrite -
     *      `{function(string url[, object options])}`
     *    – Specifies that the request is to be rewritten and then passed through. Like 'forward', but does not demand
     *      that the url is a mock directory, this can be any url.
     *      As a second argument you can provide options: <br />
     *        { onlyReplaceDomain: true } => Only replace the domain (default: false).
     *        <br />
     *      Use this to point your front end application to your 'real' local backend
     *      Example: <br />
     *      <pre>
     *      $httpBackend.scenario('realBackend', 'GET', 'api/my/backend')
     *        .rewrite('https://localhost:3001', { onlyReplaceDomain: true} );
     *      </pre>
     *  @example
     *
     <example module="myApp">
     <file name="index.html">
     <div ng-controller="ctrl">
     <h3>My http request</h3>
     <p>This is a simplyfied example.  Please keep in mind that you would never actually define a scenario in your own production app. Instead you would define a module for your e2e.html file with a dependency to your production app.</p>
     {{response | json}}
     </div>
     </file>
     <file name="script.js">
     angular.module('myApp', ['httpbackend.mock']).controller('ctrl', function($scope, $http){
    var reportResponse = function(data){ $scope.response = data; };
		$http.get('api/my/backend').success(reportResponse).error(reportResponse);
   });
     </file>
     <file name="e2e-script.js">
     angular.module('myApp')
     .run(function($httpBackend, $location){
      // Force the demo scenario
      $location.search('myScenario', true);
      $httpBackend.scenario('myScenario', 'GET', 'api/my/backend')
        .respond({ 'my-data': 'is-mocked'});
		});
     </file>
     </example>
     */

    /**
     * @ngdoc function
     * @name scenarioDELETE
     * @methodOf httpbackend.mock.$httpBackend
     * @description
     * Creates a new backend definition for DELETE used in a specific scenario. For more info see scenario.
     *
     * @param {string|Object} name the name of the scenario. Either a string representing matching the search url key,
     * or an object with one (or more) properties representing the search url key and value pair matching to the search url key and value.
     * @param {string|RegExp} url HTTP url.
     * @param {(Object|function(Object))=} headers HTTP headers.
     * @returns {requestHandler} Returns an object with `respond`, `passThrough` and `forward` methods that controls how a matched
     * request is handled.
     */

    /**
     * @ngdoc function
     * @name scenarioPOST
     * @methodOf httpbackend.mock.$httpBackend
     * @description
     * Creates a new backend definition for POST used in a specific scenario. For more info see scenario.
     *
     * @param {string|Object} name the name of the scenario. Either a string representing matching the search url key,
     * or an object with one (or more) properties representing the search url key and value pair matching to the search url key and value.
     * @param {string|RegExp} url HTTP url.
     * @param {(string|RegExp|function(string))=} data HTTP request body or function that receives
     *   data string and returns true if the data is as expected.
     * @param {(Object|function(Object))=} headers HTTP headers.
     * @returns {requestHandler} Returns an object with `respond`, `passThrough` and `forward` methods that controls how a matched
     * request is handled.
     */

    /**
     * @ngdoc function
     * @name scenarioPUT
     * @methodOf httpbackend.mock.$httpBackend
     * @description
     * Creates a new backend definition for PUT used in a specific scenario. For more info see scenario.
     *
     * @param {string|Object} name the name of the scenario. Either a string representing matching the search url key,
     * or an object with one (or more) properties representing the search url key and value pair matching to the search url key and value.
     * @param {string|RegExp} url HTTP url.
     * @param {(string|RegExp|function(string))=} data HTTP request body or function that receives
     *   data string and returns true if the data is as expected.
     * @param {(Object|function(Object))=} headers HTTP headers.
     * @returns {requestHandler} Returns an object with `respond`, `passThrough` and `forward` methods that controls how a matched
     * request is handled.
     */

    /**
     * @ngdoc function
     * @name scenarioJSONP
     * @methodOf httpbackend.mock.$httpBackend
     * @description
     * Creates a new backend definition for JSONP used in a specific scenario. For more info see scenario.
     *
     * @param {string|Object} name the name of the scenario. Either a string representing matching the search url key,
     * or an object with one (or more) properties representing the search url key and value pair matching to the search url key and value.
     * @param {string|RegExp} url HTTP url.
     * @returns {requestHandler} Returns an object with `respond`, `passThrough` and `forward` methods that controls how a matched
     * request is handled.
     **/

    /**
     * @ngdoc function
     * @name scenarioPATCH
     * @methodOf httpbackend.mock.$httpBackend
     * @description
     * Creates a new backend definition for PATCH used in a specific scenario. For more info see scenario.
     *
     * @param {string|Object} name the name of the scenario. Either a string representing matching the search url key,
     * or an object with one (or more) properties representing the search url key and value pair matching to the search url key and value.
     * @param {string|RegExp} url HTTP url.
     * @returns {requestHandler} Returns an object with `respond`, `passThrough` and `forward` methods that controls how a matched
     * request is handled.
     */
    function publishScenarioMethods(proxy) {
      proxy.scenario = scenarioManager.scenario;
      var httpMethodsWithoutData = ['GET', 'DELETE', 'JSONP'];
      var httpMethodsWithData = ['PUT', 'POST', 'PATCH'];
      angular.forEach(httpMethodsWithoutData, function (method) {
        proxy['scenario' + method] = function (name, url, headers) {
          return proxy.scenario(name, method, url, undefined, headers);
        };
      });
      angular.forEach(httpMethodsWithData, function (method) {
        proxy['scenario' + method] = function (name, url, data, headers) {
          return proxy.scenario(name, method, url, data, headers);
        };
      });
    }
  }
})();

(function () {
  'use strict';
  angular.module('httpbackend.mock').service('scenarioManager', scenarioManager);

  function scenarioManager() {
    var definitions = [];

    function scenario(name, method, url, data, headers) {
      var definition = new MockHttpScenarioExpectation(name, method, url, data, headers),
        chain = {
          respond: function (status, data, headers) {
            definition.response = createResponse(status, data, headers);
          },
          forward: function (mockFileDirectory, method) {
            if (!method) {
              method = 'GET';
            }
            definition.forwardMockFileDirectory = mockFileDirectory;
            definition.method = method;
          },
          passThrough: function () {
            definition.passThrough = true;
          },
          rewrite: function (url, options) {
            definition.rewrite = {url: url, options: options};
          }
        };

      definitions.push(definition);
      return chain;
    }

    function splitDomain(url) {
      var protocolSplit = url.split('//'),
        getIndexOfPath = function (url) {
          indexPath = url.indexOf('/');
          if (indexPath === -1) {
            indexPath = url.length;
          }
          return indexPath;
        },
        indexPath = getIndexOfPath(protocolSplit[0]),
        domain = protocolSplit[0].substr(0, indexPath),
        pathAndQuery = protocolSplit[0].substr(indexPath);
      if (protocolSplit.length === 2) {
        indexPath = getIndexOfPath(protocolSplit[1]);
        domain = protocolSplit[0] + '//' + protocolSplit[1].substr(0, indexPath);
        pathAndQuery = protocolSplit[1].substr(indexPath);
      }
      return [domain, pathAndQuery];
    }

    function createUrlByRewritingDomain(actualUrl, templateDomain) {
      var actualDomainSplit = splitDomain(actualUrl),
        templateDomainSplit = splitDomain(templateDomain);
      return templateDomainSplit[0] + actualDomainSplit[1];
    }

    function createForwardUrl(definition, searchParams) {
      var url = definition.forwardMockFileDirectory;
      if (url.substr(url.length - 1, 1) !== '/') {
        url += '/';
      }
      url += definition.getScenarioValue(searchParams);
      url += '.json';
      return url;
    }

    /**
     * @description
     * Retrieves a predefined scenario response matching to given query string parameters (search parameters) and request arguments.
     *
     * @param {Object} searchParams The search params on the current url (i.e. $location.search())
     * @param {string} method Given request HTTP method
     * @param {string} url Given request url
     * @param {Object|string} data Given request data
     * @param {Object} headers Given request headers
     *
     * @returns {Object|undefined} Returns an object containing the matched scenario response or undefined if no scenario response was specified for given search parameters and request parameters.
     * object contains
     *
     *   - name: {string} the name of the matched scenario
     *   - response: {array} (optional): an array containing the predefined response,
     *   - forward: {string} (optional): the pass through url to follow
     */
    function getScenarioResponse(searchParams, method, url, data, headers) {
      var i = -1, definition;
      while ((definition = definitions[++i])) {
        if (definition.match(searchParams, method, url, data, headers || {})) {
          var scenarioResponse = {name: definition.getScenarioName(searchParams)};
          if (definition.response) {
            scenarioResponse.response = definition.response(method, url, data, headers);
          }
          else if (definition.passThrough) {
            scenarioResponse.forward = url;
          }
          else if (definition.forwardMockFileDirectory) {
            scenarioResponse.forward = createForwardUrl(definition, searchParams);
            scenarioResponse.method = definition.method;
          }
          else if (definition.rewrite && definition.rewrite.url) {
            var rewriteOptions = angular.extend({onlyReplaceDomain: false}, definition.rewrite.options);
            if (rewriteOptions.onlyReplaceDomain) {
              scenarioResponse.forward = createUrlByRewritingDomain(url, definition.rewrite.url);
            } else {
              scenarioResponse.forward = definition.rewrite.url;
            }
          }
          else {
            throw new Error('No response defined !');
          }
          return scenarioResponse;
        }
      }
    }

    function createResponse(status, data, headers) {
      if (angular.isFunction(status)) {
        return status;
      }

      return function () {
        return angular.isNumber(status) ? [status, data, headers] : [200, status, data];
      };
    }

    function MockHttpScenarioExpectation(name, method, url, data, headers) {

      var _this = this;
      this.data = data;
      this.headers = headers;
      this.name = name;

      this.match = function (searchParams, m, u, d, h) {
        if (method !== m) {
          return false;
        }
        if (!this.matchName(searchParams)) {
          return false;
        }
        if (!this.matchUrl(u)) {
          return false;
        }
        if (angular.isDefined(d) && !this.matchData(d)) {
          return false;
        }
        if (angular.isDefined(h) && !this.matchHeaders(h)) {
          return false;
        }
        return true;
      };

      this.matchUrl = function (u) {
        if (!url) {
          return true;
        }
        if (angular.isFunction(url.test)) {
          return url.test(u);
        }
        return url === u;
      };

      this.matchHeaders = function (h) {
        if (angular.isUndefined(headers)) {
          return true;
        }
        if (angular.isFunction(headers)) {
          return headers(h);
        }
        return angular.equals(headers, h);
      };

      this.matchData = function (d) {
        if (angular.isUndefined(data)) {
          return true;
        }
        if (data && angular.isFunction(data.test)) {
          return data.test(d);
        }
        if (data && angular.isFunction(data)) {
          return data(d);
        }
        if (data && !angular.isString(data)) {
          return angular.equals(data, angular.fromJson(d));
        }
        /*jshint -W116*/
        return data == d;
      };

      this.matchName = function (searchParams) {
        return angular.isDefined(_this.getScenarioValue(searchParams));
      };

      this.getScenarioValue = function (searchParams) {
        var scenarioName = _this.getScenarioName(searchParams);
        if (angular.isObject(this.name)) {
          if (searchParams[scenarioName] === this.name[scenarioName]) {
            return searchParams[scenarioName];
          }
        } else {
          return searchParams[scenarioName];
        }
      };

      this.getScenarioName = function (searchParams) {
        if (angular.isObject(this.name)) {
          for (var key in this.name) {
            if (angular.isDefined(searchParams[key])) {
              return key;
            }
          }
        } else {
          return this.name;
        }
      };
    }

    return {
      getResponse: getScenarioResponse,
      scenario: scenario,
      getDefinedScenarios: function () {
        return definitions;
      }
    };
  }
})();
/**
 * Our very own wrapper for window.setTimeout.
 * We cannot use $timeout here, because it will pause our e2e tests while a delay is in progress.
 * See http://stackoverflow.com/questions/17185378/e2e-testing-and-timeout for more info
 */
(function () {
  'use strict';

  angular.module('httpbackend.mock').factory('timeout', timeout);

  timeout.$inject = ['$window'];

  function timeout($window) {
    var queue = [],
      timeout = function (fn, milliseconds) {
        var functionExecuted = false,
          wrappedFn = function () {
            if (!functionExecuted) {
              functionExecuted = true;
              fn();
            }
          };
        queue.push(wrappedFn);
        if (!milliseconds) {
          wrappedFn();
        } else {
          $window.setTimeout(wrappedFn, milliseconds);
        }
      };

    timeout.flush = function () {
      var fn;
      while ((fn = queue.shift())) {
        fn();
      }
    };
    return timeout;
  }
})();

