(function () {
  var app = angular.module('backMock', ['httpbackend.mock']);

  app.run(runFuction);

  runFuction.$inject = ['$httpBackend'];

  function runFuction($httpBackend) {
    $httpBackend.whenGET(/.*/).passThrough();
    $httpBackend.whenPOST(/.*/).passThrough();
  }
})();
