// Build a response
var msg = { data: []};

if(request.params.name === 'items') {
  msg.data = [
    {name: 'Onder gras', description: 'Bleek een addertje te behuizen', image: '/images/gras1.jpg'},
    {name: 'Verwijdering', description: 'We hebben er geen gras over laten groeien', image: '/images/gras2.jpg'},
    {name: 'Naald in een hooiberg', description: 'Niet gevonden waren te vroeg', image: '/images/gras3.jpg'},
    {name: 'Onderzoek bij de buurman', description: 'Het leek alleen groener', image: '/images/gras4.jpg'},
    {name: 'Migratie', description: 'Bruin- naar groengras migratie, oplossing: Water', image: '/images/gras5.jpg'}
  ];
}

if(request.params.name === 'validUserIds') {
  msg.data = [2,4,5];
}

if(request.params.name === 'users') {
  var array = request.params.ids.split(',');

  if(array.indexOf(1) > -1 || array.indexOf('1') > -1){
    msg.data.push({
      name: 'Aard de Bei',
      quote: 'Ik wil het gras niet voor je voeten wegmaaien, alleen hoor ik hier niet te staan'
    });
  }

  if(array.indexOf(2) > -1 || array.indexOf('2') > -1){
    msg.data.push({
      name: 'B.L. Oemen',
      quote: 'Begon hier zo groen als gras'
    });
  }

  if(array.indexOf(3) > -1 || array.indexOf('3') > -1){
    msg.data.push({
      name: 'C. Amgras',
      quote: 'Ik wil het gras niet voor je voeten wegmaaien, alleen hoor ik hier niet te staan'
    });
  }

  if(array.indexOf(4) > -1 || array.indexOf('4') > -1){
    msg.data.push({
      name: 'Duin. Gras',
      quote: 'Gestopt met het hele "te hooi en te gras"'
    });
  }

  if(array.indexOf(5) > -1 || array.indexOf('5') > -1){
    msg.data.push({
      name: 'Emmer Tarwe',
      quote: 'Ze nemen nooit te veel hooi op hun vork."'
    });
  }
}

if(request.params.name === 'errorPage') {
  response.status = 404;
  msg.data = "Pagina ... is als vermist opgegeven."
}

if(request.params.name === 'errorServer') {
  response.status = 500;
  msg.data = "De server heeft issues..."
}

response.body = msg;

// Forward a request
// request.forwardTo = 'http://example.com/api';
